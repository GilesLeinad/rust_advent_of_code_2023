use std::collections::HashMap;

const DAY: u32 = 5;

struct Seed {
    value: u128,
    state: String,
}

struct SeedRange {
    start: u128,
    end: u128,
}

struct Mapper {
    dest_start: u128,
    src_start: u128,
    range: u128,
}

impl Mapper {
    fn is_in_range(&self, value: u128) -> bool {
        self.src_start <= value && value < self.src_start + self.range
    }

    fn map(&self, value: u128) -> u128 {
        value - self.src_start + self.dest_start
    }
}

struct Mapping {
    mappers: Vec<Mapper>,
    next_stat: String,
}

pub fn run() {
    let mut path = std::env::current_dir().unwrap();
    path.push("Input");
    path.push(format!("Day{}.txt", DAY));
    let input = std::fs::read_to_string(&path).unwrap();
    println!("Solution for day {}:", DAY);
    println!("  Part 1: {}", solution_for_part1(&input));
    println!("  Part 2: {}", solution_for_part2(&input));
}

fn solution_for_part1(input: &str) -> u128 {
    let mut seeds = vec![];
    let mut mappings = HashMap::<String, Mapping>::new();

    let mut iterator = input.lines();

    while let Some(line) = iterator.next() {
        if line.starts_with("seeds:") {
            seeds = line
                .replace("seeds: ", "")
                .split(' ')
                .into_iter()
                .map(|x| Seed {
                    value: x.trim().parse::<u128>().unwrap(),
                    state: "seed".to_string(),
                })
                .collect::<Vec<Seed>>();
        }

        if line.ends_with("map:") {
            let map_removed = line.replace(" map:", "");
            let split = map_removed.split('-').collect::<Vec<&str>>();
            let from = split[0].to_string();
            let to = split[2].to_string();

            let mut mappers = Vec::<Mapper>::new();

            let mut line = iterator.next();

            while line.is_some() && !line.unwrap().is_empty() {
                let split = line
                    .unwrap()
                    .split(' ')
                    .into_iter()
                    .map(|x| x.trim().parse::<u128>().unwrap())
                    .collect::<Vec<u128>>();
                let destination_start = split[0];
                let source_start = split[1];
                let range = split[2];

                mappers.push(Mapper {
                    dest_start: destination_start,
                    src_start: source_start,
                    range,
                });

                line = iterator.next();
            }

            let mapping = Mapping {
                mappers,
                next_stat: to,
            };

            mappings.insert(from, mapping);
        }
    }

    for seed in seeds.iter_mut() {
        while seed.state != "location" {
            let mapping = mappings.get(&seed.state).unwrap();

            let mut new_value = seed.value;

            for mapper in &mapping.mappers {
                if mapper.is_in_range(seed.value) {
                    new_value = mapper.map(seed.value);
                }
            }

            seed.state = mapping.next_stat.clone();
            seed.value = new_value;
        }
    }

    seeds.into_iter().map(|x| x.value).min().unwrap()
}

fn solution_for_part2(input: &str) -> u128 {
    let mut seed_ranges = vec![];
    let mut mappings = HashMap::<String, Mapping>::new();

    let mut iterator = input.lines();

    while let Some(line) = iterator.next() {
        if line.starts_with("seeds:") {
            let raw_numbers = line
                .replace("seeds: ", "")
                .split(' ')
                .into_iter()
                .map(|x| x.trim().parse::<u128>().unwrap())
                .collect::<Vec<u128>>();

            for i in (0..raw_numbers.len()).step_by(2) {
                let start = raw_numbers[i];
                let end = start + raw_numbers[i + 1];

                seed_ranges.push(SeedRange { start, end });
            }
        }

        if line.ends_with("map:") {
            let map_removed = line.replace(" map:", "");
            let split = map_removed.split('-').collect::<Vec<&str>>();
            let from = split[0].to_string();
            let to = split[2].to_string();

            let mut mappers = Vec::<Mapper>::new();

            let mut line = iterator.next();

            while line.is_some() && !line.unwrap().is_empty() {
                let split = line
                    .unwrap()
                    .split(' ')
                    .into_iter()
                    .map(|x| x.trim().parse::<u128>().unwrap())
                    .collect::<Vec<u128>>();
                let destination_start = split[0];
                let source_start = split[1];
                let range = split[2];

                mappers.push(Mapper {
                    dest_start: destination_start,
                    src_start: source_start,
                    range,
                });

                line = iterator.next();
            }

            let mapping = Mapping {
                mappers,
                next_stat: to,
            };

            mappings.insert(from, mapping);
        }
    }

    let mut minimum = u128::MAX;

    for seed_range in seed_ranges {
        for i in seed_range.start..seed_range.end {
            let mut seed = Seed {
                value: i,
                state: "seed".to_string(),
            };

            while seed.state != "location" {
                let mapping = mappings.get(&seed.state).unwrap();

                let mut new_value = seed.value;

                'mapper: for mapper in &mapping.mappers {
                    if mapper.is_in_range(seed.value) {
                        new_value = mapper.map(seed.value);
                        break 'mapper;
                    }
                }

                seed.state = mapping.next_stat.clone();
                seed.value = new_value;
            }

            minimum = std::cmp::min(minimum, seed.value);
        }
    }

    minimum
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part1() {
        let mut path = std::env::current_dir().unwrap();
        path.push("TestInput");
        path.push(format!("Day{}.txt", DAY));
        let input = std::fs::read_to_string(&path).unwrap();
        assert_eq!(solution_for_part1(&input), 35);
    }

    #[test]
    fn test_part2() {
        let mut path = std::env::current_dir().unwrap();
        path.push("TestInput");
        path.push(format!("Day{}.txt", DAY));
        let input = std::fs::read_to_string(&path).unwrap();
        assert_eq!(solution_for_part2(&input), 46);
    }

    #[test]
    fn test_final_solution() {
        let mut path = std::env::current_dir().unwrap();
        path.push("Input");
        path.push(format!("Day{}.txt", DAY));

        assert_eq!(
            solution_for_part1(&std::fs::read_to_string(&path).unwrap()),
            836040384
        );
        assert_eq!(
            solution_for_part2(&std::fs::read_to_string(&path).unwrap()),
            10834440
        );
    }
}
