const DAY: u32 = 6;

pub fn run() {
    let mut path = std::env::current_dir().unwrap();
    path.push("Input");
    path.push(format!("Day{}.txt", DAY));
    let input = std::fs::read_to_string(&path).unwrap();
    println!("Solution for day {}:", DAY);
    println!("  Part 1: {}", solution_for_part1(&input));
    println!("  Part 2: {}", solution_for_part2(&input));
}

fn solution_for_part1(input: &str) -> usize {
    let times = input
        .lines()
        .next()
        .unwrap()
        .replace("Time:", "")
        .trim()
        .split(' ')
        .filter(|x| !x.is_empty())
        .map(|x| x.trim().parse::<usize>().unwrap())
        .collect::<Vec<usize>>();
    let distances = input
        .lines()
        .nth(1)
        .unwrap()
        .replace("Distance:", "")
        .trim()
        .split(' ')
        .filter(|x| !x.is_empty())
        .map(|x| x.trim().parse::<usize>().unwrap())
        .collect::<Vec<usize>>();

    let mut possible_wins = Vec::<usize>::new();

    for (time, distance) in times.iter().zip(distances.iter()) {
        let possible_win = calc_number_possible_win(time, distance);
        possible_wins.push(possible_win);
    }
    possible_wins.iter().product()
}

fn calc_number_possible_win(time: &usize, distance: &usize) -> usize {
    let mut count = 0;
    for i in 0..=*time {
        let velocity = i;
        let driving_time = *time - i;
        if velocity * driving_time > *distance {
            count += 1
        }
    }
    count
}

fn solution_for_part2(input: &str) -> usize {
    let time = input
        .lines()
        .next()
        .unwrap()
        .replace("Time:", "")
        .trim()
        .replace(' ', "")
        .parse::<usize>()
        .unwrap();
    let distance = input
        .lines()
        .nth(1)
        .unwrap()
        .replace("Distance:", "")
        .trim()
        .replace(' ', "")
        .parse::<usize>()
        .unwrap();

    calc_number_possible_win(&time, &distance)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part1() {
        let mut path = std::env::current_dir().unwrap();
        path.push("TestInput");
        path.push(format!("Day{}.txt", DAY));
        let input = std::fs::read_to_string(&path).unwrap();
        assert_eq!(solution_for_part1(&input), 288);
    }

    #[test]
    fn test_part2() {
        let mut path = std::env::current_dir().unwrap();
        path.push("TestInput");
        path.push(format!("Day{}.txt", DAY));
        let input = std::fs::read_to_string(&path).unwrap();
        assert_eq!(solution_for_part2(&input), 71503);
    }

    #[test]
    fn test_final_solution() {
        let mut path = std::env::current_dir().unwrap();
        path.push("Input");
        path.push(format!("Day{}.txt", DAY));

        assert_eq!(
            solution_for_part1(&std::fs::read_to_string(&path).unwrap()),
            449820
        );
        assert_eq!(
            solution_for_part2(&std::fs::read_to_string(&path).unwrap()),
            42250895
        );
    }
}
