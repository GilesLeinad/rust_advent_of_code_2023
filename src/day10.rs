const DAY: u32 = 10;

pub fn run() {
    let mut path = std::env::current_dir().unwrap();
    path.push("Input");
    path.push(format!("Day{}.txt", DAY));
    let input = std::fs::read_to_string(&path).unwrap();
    println!("Solution for day {}:", DAY);
    println!("  Part 1: {}", solution_for_part1(&input));
    println!("  Part 2: {}", solution_for_part2(&input));
}

#[derive(Clone, PartialEq)]
enum Category {
    Unknown,
    Loop,
    Left,
    Right,
}

#[derive(Clone)]
struct Field {
    category: Category,
    content: char,
    coordinates: (usize, usize),
}

struct Map {
    fields: Vec<Vec<Field>>,
    width: usize,
    height: usize,
    start: (usize, usize),
}

impl Map {
    pub fn has_unknown(&self) -> bool {
        for line in self.fields.iter() {
            for field in line {
                if field.category == Category::Unknown {
                    return true;
                }
            }
        }
        false
    }
}

fn solution_for_part1(input: &str) -> usize {
    let mut map = build_map(input);
    walk_loop(&mut map)
}

fn walk_loop(map: &mut Map) -> usize {
    let mut loop_size = 1;
    let mut current_field = map.start;
    let mut previous_field = map.start;

    'dir: for dir in [(1, 0), (0, 1), (-1, 0), (0, -1)] {
        let field_to_check_coordinates = (
            (current_field.0 as i32 + dir.0) as usize,
            (current_field.1 as i32 + dir.1) as usize,
        );

        let field_to_check =
            &map.fields[field_to_check_coordinates.0][field_to_check_coordinates.1];

        match field_to_check.content {
            'J' | '-' | '7' if dir == (1, 0) => {
                current_field = field_to_check_coordinates;
                break 'dir;
            }
            'J' | '|' | 'L' if dir == (0, 1) => {
                current_field = field_to_check_coordinates;
                break 'dir;
            }
            'L' | '-' | 'F' if dir == (-1, 0) => {
                current_field = field_to_check_coordinates;
                break 'dir;
            }
            '7' | '|' | 'F' if dir == (0, -1) => {
                current_field = field_to_check_coordinates;
                break 'dir;
            }
            _ => {}
        }
    }
    map.fields[current_field.0][current_field.1].category = Category::Loop;

    while current_field != map.start {
        let current_field_content = map.fields[current_field.0][current_field.1].content;
        let direction: (i32, i32) = (
            current_field.0 as i32 - previous_field.0 as i32,
            current_field.1 as i32 - previous_field.1 as i32,
        );
        previous_field = current_field;

        match current_field_content {
            '|' => {
                if direction.1 > 0 {
                    current_field.1 += 1;
                } else {
                    current_field.1 -= 1;
                }
            }
            '-' => {
                if direction.0 > 0 {
                    current_field.0 += 1;
                } else {
                    current_field.0 -= 1;
                }
            }
            'J' => {
                if direction.0 > 0 {
                    current_field.1 -= 1;
                } else {
                    current_field.0 -= 1;
                }
            }
            '7' => {
                if direction.0 > 0 {
                    current_field.1 += 1;
                } else {
                    current_field.0 -= 1;
                }
            }
            'L' => {
                if direction.1 > 0 {
                    current_field.0 += 1;
                } else {
                    current_field.1 -= 1;
                }
            }
            'F' => {
                if direction.0 < 0 {
                    current_field.1 += 1;
                } else {
                    current_field.0 += 1;
                }
            }
            _ => {}
        }
        map.fields[current_field.0][current_field.1].category = Category::Loop;
        loop_size += 1
    }

    loop_size / 2
}

fn build_map(input: &str) -> Map {
    let width = input.lines().next().unwrap().len();
    let height = input.lines().count();
    let mut start = (0_usize, 0_usize);

    let mut fields = vec![
        vec![
            Field {
                category: Category::Unknown,
                content: ' ',
                coordinates: (0, 0),
            };
            height
        ];
        width
    ];

    for (line_number, line) in input.lines().enumerate() {
        for (char_number, char) in line.chars().enumerate() {
            fields[char_number][line_number].content = char;
            fields[char_number][line_number].coordinates = (char_number, line_number);
            if char == 'S' {
                fields[char_number][line_number].category = Category::Loop;
                start = (char_number, line_number);
            }
        }
    }

    Map {
        fields,
        width,
        height,
        start,
    }
}

fn solution_for_part2(input: &str) -> usize {
    let mut map = build_map(input);
    walk_loop(&mut map);
    categorize_left_right(&mut map);
    fill_empty(&mut map);
    let mut count_left = 0;
    let mut count_right = 0;

    for line in map.fields.iter() {
        for field in line.iter() {
            if field.category == Category::Left {
                count_left += 1;
            }
            if field.category == Category::Right {
                count_right += 1;
            }
        }
    }

    let outside = get_outside(&map);

    if outside == Category::Left {
        return count_right;
    }

    count_left
}

fn get_outside(map: &Map) -> Category {
    for i in 0..map.width {
        if map.fields[i][0].category == Category::Left
            || map.fields[i][map.height - 1].category == Category::Left
        {
            return Category::Left;
        }
    }
    for i in 0..map.height {
        if map.fields[0][i].category == Category::Left
            || map.fields[map.width - 1][i].category == Category::Left
        {
            return Category::Left;
        }
    }
    Category::Right
}

fn fill_empty(map: &mut Map) {
    while map.has_unknown() {
        for line in map.fields.clone() {
            for field in line {
                if field.category == Category::Left {
                    for i in [(0, 1), (1, 0), (0, -1), (-1, 0)] {
                        if (field.coordinates.0 as i32 + i.0) >= 0
                            && (field.coordinates.0 as i32 + i.0) < map.fields.len() as i32
                            && (field.coordinates.1 as i32 + i.1) >= 0
                            && (field.coordinates.1 as i32 + i.1) < map.fields[0].len() as i32
                        {
                            let coordinates = (
                                (field.coordinates.0 as i32 + i.0) as usize,
                                (field.coordinates.1 as i32 + i.1) as usize,
                            );
                            if map.fields[coordinates.0][coordinates.1].category
                                == Category::Unknown
                            {
                                map.fields[coordinates.0][coordinates.1].category = Category::Left;
                            }
                        }
                    }
                }
                if field.category == Category::Right {
                    for i in [(0, 1), (1, 0), (0, -1), (-1, 0)] {
                        if (field.coordinates.0 as i32 + i.0) >= 0
                            && (field.coordinates.0 as i32 + i.0) < map.fields.len() as i32
                            && (field.coordinates.1 as i32 + i.1) >= 0
                            && (field.coordinates.1 as i32 + i.1) < map.fields[0].len() as i32
                        {
                            let coordinates = (
                                (field.coordinates.0 as i32 + i.0) as usize,
                                (field.coordinates.1 as i32 + i.1) as usize,
                            );
                            if map.fields[coordinates.0][coordinates.1].category
                                == Category::Unknown
                            {
                                map.fields[coordinates.0][coordinates.1].category = Category::Right;
                            }
                        }
                    }
                }
            }
        }
    }
}

fn categorize_left_right(map: &mut Map) {
    let mut current_field = map.start;
    let mut previous_field = map.start;

    'dir: for dir in [(1, 0), (0, 1), (-1, 0), (0, -1)] {
        let field_to_check_coordinates = (
            (current_field.0 as i32 + dir.0) as usize,
            (current_field.1 as i32 + dir.1) as usize,
        );

        let field_to_check =
            &map.fields[field_to_check_coordinates.0][field_to_check_coordinates.1];

        match field_to_check.content {
            'J' | '-' | '7' if dir == (1, 0) => {
                current_field = field_to_check_coordinates;
                break 'dir;
            }
            'J' | '|' | 'L' if dir == (0, 1) => {
                current_field = field_to_check_coordinates;
                break 'dir;
            }
            'L' | '-' | 'F' if dir == (-1, 0) => {
                current_field = field_to_check_coordinates;
                break 'dir;
            }
            '7' | '|' | 'F' if dir == (0, -1) => {
                current_field = field_to_check_coordinates;
                break 'dir;
            }
            _ => {}
        }
    }

    loop {
        let current_field_content = map.fields[current_field.0][current_field.1].content;
        let direction: (i32, i32) = (
            current_field.0 as i32 - previous_field.0 as i32,
            current_field.1 as i32 - previous_field.1 as i32,
        );
        previous_field = current_field;

        match current_field_content {
            '|' => {
                if direction.1 > 0 {
                    if current_field.0 + 1 < map.fields.len()
                        && map.fields[current_field.0 + 1][current_field.1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0 + 1][current_field.1].category = Category::Left;
                    }
                    if current_field.0 > 0
                        && map.fields[current_field.0 - 1][current_field.1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0 - 1][current_field.1].category = Category::Right;
                    }
                    current_field.1 += 1;
                } else {
                    if current_field.0 + 1 < map.fields.len()
                        && map.fields[current_field.0 + 1][current_field.1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0 + 1][current_field.1].category = Category::Right;
                    }
                    if current_field.0 > 0
                        && map.fields[current_field.0 - 1][current_field.1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0 - 1][current_field.1].category = Category::Left;
                    }
                    current_field.1 -= 1;
                }
            }
            '-' => {
                if direction.0 > 0 {
                    if current_field.1 > 0
                        && map.fields[current_field.0][current_field.1 - 1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0][current_field.1 - 1].category = Category::Left;
                    }
                    if current_field.1 + 1 < map.fields[current_field.0].len()
                        && map.fields[current_field.0][current_field.1 + 1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0][current_field.1 + 1].category = Category::Right;
                    }
                    current_field.0 += 1;
                } else {
                    if current_field.1 > 0
                        && map.fields[current_field.0][current_field.1 - 1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0][current_field.1 - 1].category = Category::Right;
                    }
                    if current_field.1 + 1 < map.fields[current_field.0].len()
                        && map.fields[current_field.0][current_field.1 + 1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0][current_field.1 + 1].category = Category::Left;
                    }
                    current_field.0 -= 1;
                }
            }
            'J' => {
                if direction.0 > 0 {
                    if current_field.0 + 1 < map.fields.len()
                        && map.fields[current_field.0 + 1][current_field.1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0 + 1][current_field.1].category = Category::Right
                    }
                    if current_field.1 + 1 < map.fields[current_field.0].len()
                        && map.fields[current_field.0][current_field.1 + 1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0][current_field.1 + 1].category = Category::Right
                    }
                    if current_field.0 + 1 < map.fields.len()
                        && current_field.1 + 1 < map.fields[current_field.0].len()
                        && map.fields[current_field.0 + 1][current_field.1 + 1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0 + 1][current_field.1 + 1].category =
                            Category::Right
                    }
                    if current_field.0 > 0
                        && current_field.1 > 0
                        && map.fields[current_field.0 - 1][current_field.1 - 1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0 - 1][current_field.1 - 1].category =
                            Category::Left
                    }
                    current_field.1 -= 1;
                } else {
                    if current_field.0 + 1 < map.fields.len()
                        && map.fields[current_field.0 + 1][current_field.1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0 + 1][current_field.1].category = Category::Left
                    }
                    if current_field.1 + 1 < map.fields[current_field.0].len()
                        && map.fields[current_field.0][current_field.1 + 1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0][current_field.1 + 1].category = Category::Left
                    }
                    if current_field.0 + 1 < map.fields.len()
                        && current_field.1 + 1 < map.fields[current_field.0].len()
                        && map.fields[current_field.0 + 1][current_field.1 + 1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0 + 1][current_field.1 + 1].category =
                            Category::Left
                    }
                    if current_field.0 > 0
                        && current_field.1 > 0
                        && map.fields[current_field.0 - 1][current_field.1 - 1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0 - 1][current_field.1 - 1].category =
                            Category::Right
                    }
                    current_field.0 -= 1;
                }
            }
            '7' => {
                if direction.0 > 0 {
                    if current_field.0 + 1 < map.fields.len()
                        && map.fields[current_field.0 + 1][current_field.1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0 + 1][current_field.1].category = Category::Left
                    }
                    if current_field.1 > 0
                        && map.fields[current_field.0][current_field.1 - 1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0][current_field.1 - 1].category = Category::Left
                    }
                    if current_field.0 + 1 < map.fields.len()
                        && current_field.1 > 0
                        && map.fields[current_field.0 + 1][current_field.1 - 1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0 + 1][current_field.1 - 1].category =
                            Category::Left
                    }
                    if current_field.0 > 0
                        && current_field.1 + 1 < map.fields[0].len()
                        && map.fields[current_field.0 - 1][current_field.1 + 1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0 - 1][current_field.1 + 1].category =
                            Category::Right
                    }
                    current_field.1 += 1;
                } else {
                    if current_field.0 + 1 < map.fields.len()
                        && map.fields[current_field.0 + 1][current_field.1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0 + 1][current_field.1].category = Category::Right
                    }
                    if current_field.1 > 0
                        && map.fields[current_field.0][current_field.1 - 1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0][current_field.1 - 1].category = Category::Right
                    }
                    if current_field.0 + 1 < map.fields.len()
                        && current_field.1 > 0
                        && map.fields[current_field.0 + 1][current_field.1 - 1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0 + 1][current_field.1 - 1].category =
                            Category::Right
                    }
                    if current_field.0 > 0
                        && current_field.1 + 1 < map.fields[0].len()
                        && map.fields[current_field.0 - 1][current_field.1 + 1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0 - 1][current_field.1 + 1].category =
                            Category::Left
                    }
                    current_field.0 -= 1;
                }
            }
            'L' => {
                if direction.1 > 0 {
                    if current_field.0 > 0
                        && map.fields[current_field.0 - 1][current_field.1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0 - 1][current_field.1].category = Category::Right
                    }
                    if current_field.0 > 0
                        && current_field.1 + 1 < map.fields[current_field.0].len()
                        && map.fields[current_field.0 - 1][current_field.1 + 1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0 - 1][current_field.1 + 1].category =
                            Category::Right
                    }
                    if current_field.1 + 1 < map.fields[current_field.0].len()
                        && map.fields[current_field.0][current_field.1 + 1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0][current_field.1 + 1].category = Category::Right
                    }
                    if current_field.1 > 0
                        && current_field.0 + 1 < map.fields.len()
                        && map.fields[current_field.0 + 1][current_field.1 - 1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0 + 1][current_field.1 - 1].category =
                            Category::Left
                    }
                    current_field.0 += 1;
                } else {
                    if current_field.0 > 0
                        && map.fields[current_field.0 - 1][current_field.1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0 - 1][current_field.1].category = Category::Left
                    }
                    if current_field.0 > 0
                        && current_field.1 + 1 < map.fields[current_field.0].len()
                        && map.fields[current_field.0 - 1][current_field.1 + 1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0 - 1][current_field.1 + 1].category =
                            Category::Left
                    }
                    if current_field.1 + 1 < map.fields[current_field.0].len()
                        && map.fields[current_field.0][current_field.1 + 1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0][current_field.1 + 1].category = Category::Left
                    }
                    if current_field.1 > 0
                        && current_field.0 + 1 < map.fields.len()
                        && map.fields[current_field.0 + 1][current_field.1 - 1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0 + 1][current_field.1 - 1].category =
                            Category::Right
                    }
                    current_field.1 -= 1;
                }
            }
            'F' => {
                if direction.0 < 0 {
                    if current_field.0 > 0
                        && map.fields[current_field.0 - 1][current_field.1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0 - 1][current_field.1].category = Category::Right
                    }
                    if current_field.1 > 0
                        && map.fields[current_field.0][current_field.1 - 1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0][current_field.1 - 1].category = Category::Right
                    }
                    if current_field.0 > 0
                        && current_field.1 > 0
                        && map.fields[current_field.0 - 1][current_field.1 - 1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0 - 1][current_field.1 - 1].category =
                            Category::Right
                    }
                    if current_field.0 + 1 < map.fields.len()
                        && current_field.1 + 1 < map.fields[current_field.0].len()
                        && map.fields[current_field.0 + 1][current_field.1 + 1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0 + 1][current_field.1 + 1].category =
                            Category::Left
                    }
                    current_field.1 += 1;
                } else {
                    if current_field.0 > 0
                        && map.fields[current_field.0 - 1][current_field.1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0 - 1][current_field.1].category = Category::Left
                    }
                    if current_field.1 > 0
                        && map.fields[current_field.0][current_field.1 - 1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0][current_field.1 - 1].category = Category::Left
                    }
                    if current_field.0 > 0
                        && current_field.1 > 0
                        && map.fields[current_field.0 - 1][current_field.1 - 1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0 - 1][current_field.1 - 1].category =
                            Category::Left
                    }
                    if current_field.0 + 1 < map.fields.len()
                        && current_field.1 + 1 < map.fields[current_field.0].len()
                        && map.fields[current_field.0 + 1][current_field.1 + 1].category
                            == Category::Unknown
                    {
                        map.fields[current_field.0 + 1][current_field.1 + 1].category =
                            Category::Right
                    }
                    current_field.0 += 1;
                }
            }
            _ => {}
        }

        if current_field == map.start {
            break;
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part1() {
        let mut path = std::env::current_dir().unwrap();
        path.push("TestInput");
        path.push(format!("Day{}_Part1.txt", DAY));
        let input = std::fs::read_to_string(&path).unwrap();
        assert_eq!(solution_for_part1(&input), 8);
    }

    #[test]
    fn test_part2() {
        let mut path = std::env::current_dir().unwrap();
        path.push("TestInput");
        path.push(format!("Day{}_Part2.txt", DAY));
        let input = std::fs::read_to_string(&path).unwrap();
        assert_eq!(solution_for_part2(&input), 4);
    }

    #[test]
    fn test_final_solution() {
        let mut path = std::env::current_dir().unwrap();
        path.push("Input");
        path.push(format!("Day{}.txt", DAY));

        assert_eq!(
            solution_for_part1(&std::fs::read_to_string(&path).unwrap()),
            6613
        );
        assert_eq!(
            solution_for_part2(&std::fs::read_to_string(&path).unwrap()),
            511
        );
    }
}
