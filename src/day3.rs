use std::collections::HashSet;

const DAY: u32 = 3;

pub fn run() {
    let mut path = std::env::current_dir().unwrap();
    path.push("Input");
    path.push(format!("Day{}.txt", DAY));
    let input = std::fs::read_to_string(&path).unwrap();
    println!("Solution for day {}:", DAY);
    println!("  Part 1: {}", solution_for_part1(&input));
    println!("  Part 2: {}", solution_for_part2(&input));
}

struct Number {
    number: usize,
    location: Location,
}

struct Location {
    start: Point,
    end: Point,
}

#[derive(Eq, Hash, PartialEq)]
struct Point {
    x: i64,
    y: i64,
}

fn solution_for_part1(input: &str) -> usize {
    let numbers = get_numbers(input);
    let symbols = get_symbols(input);

    numbers
        .into_iter()
        .filter(|number| is_number_next_to_symbol(number, &symbols))
        .map(|number| number.number)
        .sum()
}

fn is_number_next_to_symbol(number: &Number, symbols: &HashSet<Point>) -> bool {
    let surroundings = get_adjacent_fields(number);
    for surrounding in surroundings {
        if symbols.contains(&surrounding) {
            return true;
        }
    }

    false
}

fn get_numbers(input: &str) -> Vec<Number> {
    let mut numbers = Vec::<Number>::new();

    for (line_number, line) in input.lines().enumerate() {
        let mut start_index = 0_usize;
        let mut end_index = 0_usize;

        while start_index < line.len() {
            if line.chars().nth(start_index).unwrap().is_ascii_digit() {
                while end_index < line.len()
                    && line.chars().nth(end_index).unwrap().is_ascii_digit()
                {
                    end_index += 1;
                }

                let value = line
                    .get(start_index..end_index)
                    .unwrap()
                    .parse::<usize>()
                    .unwrap();

                let number = Number {
                    number: value,
                    location: Location {
                        start: Point {
                            x: start_index as i64,
                            y: line_number as i64,
                        },
                        end: Point {
                            x: (end_index - 1) as i64,
                            y: line_number as i64,
                        },
                    },
                };

                numbers.push(number);
                start_index = end_index;
            } else {
                start_index += 1;
                end_index = start_index;
            }
        }
    }

    numbers
}

fn get_symbols(input: &str) -> HashSet<Point> {
    let mut symbols = HashSet::<Point>::new();
    for (line_number, line) in input.lines().enumerate() {
        for (char_number, char) in line.chars().enumerate() {
            if !char.is_ascii_digit() && char != '.' {
                let position = Point {
                    x: char_number as i64,
                    y: line_number as i64,
                };
                symbols.insert(position);
            }
        }
    }
    symbols
}

fn solution_for_part2(input: &str) -> usize {
    let possible_gears = get_possible_gears(input);
    let numbers = get_numbers(input);
    let gear_ratios = get_gear_ratios(&possible_gears, &numbers);

    gear_ratios.into_iter().sum()
}

fn get_gear_ratios(possible_gears: &HashSet<Point>, numbers: &[Number]) -> Vec<usize> {
    possible_gears
        .iter()
        .filter_map(|possible_gear| get_gear_ratio(possible_gear, numbers))
        .collect()
}

fn get_gear_ratio(possible_gear: &Point, numbers: &[Number]) -> Option<usize> {
    let adjacent_numbers = numbers
        .iter()
        .filter(|number| get_adjacent_fields(number).contains(possible_gear))
        .collect::<Vec<&Number>>();

    if adjacent_numbers.len() != 2 {
        return None;
    }

    let result = adjacent_numbers.get(0).unwrap().number * adjacent_numbers.get(1).unwrap().number;
    Some(result)
}

fn get_adjacent_fields(number: &Number) -> HashSet<Point> {
    let start = &number.location.start;
    let end = &number.location.end;

    let around_start = [
        Point {
            x: start.x - 1,
            y: start.y - 1,
        },
        Point {
            x: start.x - 1,
            y: start.y,
        },
        Point {
            x: start.x - 1,
            y: start.y + 1,
        },
        Point {
            x: start.x,
            y: start.y - 1,
        },
        Point {
            x: start.x,
            y: start.y + 1,
        },
    ];

    let around_end = [
        Point {
            x: end.x + 1,
            y: end.y - 1,
        },
        Point {
            x: end.x + 1,
            y: end.y,
        },
        Point {
            x: end.x + 1,
            y: end.y + 1,
        },
        Point {
            x: end.x,
            y: end.y - 1,
        },
        Point {
            x: end.x,
            y: end.y + 1,
        },
    ];

    let mut surroundings = HashSet::<Point>::new();
    surroundings.extend(around_start);
    surroundings.extend(around_end);

    let elements_between = (end.x - start.x) - 1;

    if elements_between > 0 {
        for i in 0..=elements_between {
            surroundings.insert(Point {
                x: start.x + 1 + i,
                y: start.y - 1,
            });
            surroundings.insert(Point {
                x: start.x + 1 + i,
                y: start.y + 1,
            });
        }
    }

    surroundings
}

fn get_possible_gears(input: &str) -> HashSet<Point> {
    let mut possible_gears = HashSet::<Point>::new();
    for (line_number, line) in input.lines().enumerate() {
        for (char_number, char) in line.chars().enumerate() {
            if char == '*' {
                let position = Point {
                    x: char_number as i64,
                    y: line_number as i64,
                };

                possible_gears.insert(position);
            }
        }
    }
    possible_gears
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part1() {
        let mut path = std::env::current_dir().unwrap();
        path.push("TestInput");
        path.push(format!("Day{}.txt", DAY));
        let input = std::fs::read_to_string(&path).unwrap();
        assert_eq!(solution_for_part1(&input), 4361);
    }

    #[test]
    fn test_part2() {
        let mut path = std::env::current_dir().unwrap();
        path.push("TestInput");
        path.push(format!("Day{}.txt", DAY));
        let input = std::fs::read_to_string(&path).unwrap();
        assert_eq!(solution_for_part2(&input), 467835);
    }

    #[test]
    fn test_final_solution() {
        let mut path = std::env::current_dir().unwrap();
        path.push("Input");
        path.push(format!("Day{}.txt", DAY));

        assert_eq!(
            solution_for_part1(&std::fs::read_to_string(&path).unwrap()),
            525119
        );
        assert_eq!(
            solution_for_part2(&std::fs::read_to_string(&path).unwrap()),
            76504829
        );
    }
}
