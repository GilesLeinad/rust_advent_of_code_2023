use regex::Regex;
use std::cmp;
use std::collections::HashMap;

const DAY: u32 = 2;

pub fn run() {
    let mut path = std::env::current_dir().unwrap();
    path.push("Input");
    path.push(format!("Day{}.txt", DAY));
    let input = std::fs::read_to_string(&path).unwrap();
    println!("Solution for day {}:", DAY);
    println!("  Part 1: {}", solution_for_part1(&input));
    println!("  Part 2: {}", solution_for_part2(&input));
}

fn get_max_cubes(color: &str) -> Option<usize> {
    let max_cubes: HashMap<&str, usize> = HashMap::from([("red", 12), ("green", 13), ("blue", 14)]);
    if max_cubes.contains_key(color) {
        return Some(max_cubes[color]);
    }
    None
}

fn solution_for_part1(input: &str) -> usize {
    let game_re = Regex::new(r"Game (\d*): ([\w\d ,;]*)").unwrap();
    let mut possible_games = Vec::<usize>::new();

    for line in input.lines() {
        let game_cap = game_re.captures(line).unwrap();
        let game_number = game_cap[1].parse::<usize>().unwrap();
        let all_rounds = game_cap[2].trim();
        let rounds = all_rounds.split(';').collect::<Vec<&str>>();

        let mut game_possible = true;

        'round: for round in rounds.iter() {
            let colors_in_round = round.trim().split(',').collect::<Vec<&str>>();
            for color in colors_in_round.iter() {
                let color_details = color.trim().split(' ').collect::<Vec<&str>>();
                let number = color_details.first().unwrap().parse::<usize>().unwrap();
                let color = color_details.get(1).unwrap();

                let color_larger_max = number > get_max_cubes(color).unwrap();

                if color_larger_max {
                    game_possible = false;
                    break 'round;
                }
            }
        }

        if game_possible {
            possible_games.push(game_number)
        }
    }

    possible_games.iter().sum()
}

fn solution_for_part2(input: &str) -> usize {
    let mut game_powers = Vec::new();
    for game in input.lines() {
        let mut maximum_cubes_per_color: HashMap<&str, usize> = HashMap::new();

        let game_split = game.split(':').collect::<Vec<&str>>();
        let all_rounds = game_split.get(1).unwrap();
        let rounds = all_rounds.split(';').collect::<Vec<&str>>();

        for round in rounds {
            let colors_in_round = round.split(',').collect::<Vec<&str>>();
            for color in colors_in_round {
                let color_split = color.trim().split(' ').collect::<Vec<&str>>();
                let number = color_split
                    .first()
                    .unwrap()
                    .trim()
                    .parse::<usize>()
                    .unwrap();
                let name = color_split.get(1).unwrap().trim();

                maximum_cubes_per_color
                    .entry(name)
                    .and_modify(|current_max| *current_max = cmp::max(*current_max, number))
                    .or_insert(number);
            }
        }

        let game_power: usize = maximum_cubes_per_color.values().into_iter().product();
        game_powers.push(game_power);
    }
    game_powers.into_iter().sum()
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part1() {
        let mut path = std::env::current_dir().unwrap();
        path.push("TestInput");
        path.push(format!("Day{}.txt", DAY));
        let input = std::fs::read_to_string(&path).unwrap();
        assert_eq!(solution_for_part1(&input), 8);
    }

    #[test]
    fn test_part2() {
        let mut path = std::env::current_dir().unwrap();
        path.push("TestInput");
        path.push(format!("Day{}.txt", DAY));
        let input = std::fs::read_to_string(&path).unwrap();
        assert_eq!(solution_for_part2(&input), 2286);
    }

    #[test]
    fn test_final_solution() {
        let mut path = std::env::current_dir().unwrap();
        path.push("Input");
        path.push(format!("Day{}.txt", DAY));

        assert_eq!(
            solution_for_part1(&std::fs::read_to_string(&path).unwrap()),
            1853
        );
        assert_eq!(
            solution_for_part2(&std::fs::read_to_string(&path).unwrap()),
            72706
        );
    }
}
