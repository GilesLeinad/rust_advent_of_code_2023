const DAY: u32 = 9;

pub fn run() {
    let mut path = std::env::current_dir().unwrap();
    path.push("Input");
    path.push(format!("Day{}.txt", DAY));
    let input = std::fs::read_to_string(&path).unwrap();
    println!("Solution for day {}:", DAY);
    println!("  Part 1: {}", solution_for_part1(&input));
    println!("  Part 2: {}", solution_for_part2(&input));
}

fn solution_for_part1(input: &str) -> i32 {
    let mut line_values = Vec::<i32>::new();

    for raw_line in input.lines() {
        let line = raw_line
            .split(' ')
            .map(|x| x.trim().parse::<i32>().unwrap())
            .collect::<Vec<i32>>();

        let value = find_value_last(&line);
        line_values.push(value + line.last().unwrap());
    }
    line_values.iter().sum()
}

fn find_value_last(list: &Vec<i32>) -> i32 {
    if list.iter().all(|x| x == &0) {
        return 0;
    }
    let mut differences = Vec::<i32>::new();
    for i in 1..list.len() {
        let difference = list[i] - list[i - 1];
        differences.push(difference);
    }
    let value = find_value_last(&differences);

    differences.last().unwrap() + value
}

fn solution_for_part2(input: &str) -> i32 {
    let mut line_values = Vec::<i32>::new();

    for raw_line in input.lines() {
        let line = raw_line
            .split(' ')
            .map(|x| x.trim().parse::<i32>().unwrap())
            .collect::<Vec<i32>>();

        let value = find_value_first(&line);
        line_values.push(line.first().unwrap() - value);
    }
    line_values.iter().sum()
}

fn find_value_first(list: &Vec<i32>) -> i32 {
    if list.iter().all(|x| x == &0) {
        return 0;
    }
    let mut differences = Vec::<i32>::new();
    for i in 1..list.len() {
        let difference = list[i] - list[i - 1];
        differences.push(difference);
    }
    let value = find_value_first(&differences);

    differences.first().unwrap() - value
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part1() {
        let mut path = std::env::current_dir().unwrap();
        path.push("TestInput");
        path.push(format!("Day{}.txt", DAY));
        let input = std::fs::read_to_string(&path).unwrap();
        assert_eq!(solution_for_part1(&input), 114);
    }

    #[test]
    fn test_part2() {
        let mut path = std::env::current_dir().unwrap();
        path.push("TestInput");
        path.push(format!("Day{}.txt", DAY));
        let input = std::fs::read_to_string(&path).unwrap();
        assert_eq!(solution_for_part2(&input), 2);
    }

    #[test]
    fn test_final_solution() {
        let mut path = std::env::current_dir().unwrap();
        path.push("Input");
        path.push(format!("Day{}.txt", DAY));

        assert_eq!(
            solution_for_part1(&std::fs::read_to_string(&path).unwrap()),
            1684566095
        );
        assert_eq!(
            solution_for_part2(&std::fs::read_to_string(&path).unwrap()),
            1136
        );
    }
}
