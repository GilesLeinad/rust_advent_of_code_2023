use std::collections::HashMap;

use regex::Regex;

const DAY: u32 = 4;

pub fn run() {
    let mut path = std::env::current_dir().unwrap();
    path.push("Input");
    path.push(format!("Day{}.txt", DAY));
    let input = std::fs::read_to_string(&path).unwrap();
    println!("Solution for day {}:", DAY);
    println!("  Part 1: {}", solution_for_part1(&input));
    println!("  Part 2: {}", solution_for_part2(&input));
}

fn solution_for_part1(input: &str) -> usize {
    let re = Regex::new(r"Card [ \d]*: ([\d ]*) \| ([\d ]*)").unwrap();
    let mut card_values = vec![];

    for line in input.lines() {
        let capture = re.captures(line).unwrap();
        let winning_numbers = capture
            .get(1)
            .unwrap()
            .as_str()
            .trim()
            .split(' ')
            .into_iter()
            .filter_map(|x| x.trim().parse::<usize>().ok())
            .collect::<Vec<usize>>();

        let player_numbers = capture
            .get(2)
            .unwrap()
            .as_str()
            .trim()
            .split(' ')
            .into_iter()
            .filter_map(|x| x.trim().parse::<usize>().ok())
            .collect::<Vec<usize>>();

        let intersection = player_numbers
            .iter()
            .filter(|x| winning_numbers.contains(x))
            .collect::<Vec<&usize>>();

        let card_value = match intersection.len() {
            0 => 0,
            1 => 1,
            number => {
                let mut result = 1;
                for _ in 1..number {
                    result *= 2;
                }
                result
            }
        };
        card_values.push(card_value);
    }
    card_values.iter().sum()
}

fn solution_for_part2(input: &str) -> usize {
    let re = Regex::new(r"Card *(\d*): ([\d ]*) \| ([\d ]*)").unwrap();
    let mut card_stack = HashMap::<usize, usize>::new();

    let mut last_card_number = 0;

    for line in input.lines() {
        let capture = re.captures(line).unwrap();
        let card_number = capture.get(1).unwrap().as_str().parse::<usize>().unwrap();
        last_card_number = card_number;
        let number_current_card = card_stack
            .entry(card_number)
            .and_modify(|x| *x += 1)
            .or_insert(1);
        let number_current_card = number_current_card.to_owned();

        let winning_numbers = capture
            .get(2)
            .unwrap()
            .as_str()
            .trim()
            .split(' ')
            .into_iter()
            .filter_map(|x| x.trim().parse::<usize>().ok())
            .collect::<Vec<usize>>();

        let player_numbers = capture
            .get(3)
            .unwrap()
            .as_str()
            .trim()
            .split(' ')
            .into_iter()
            .filter_map(|x| x.trim().parse::<usize>().ok())
            .collect::<Vec<usize>>();

        let player_wins = player_numbers
            .iter()
            .filter(|x| winning_numbers.contains(x))
            .count();

        for i in 1..=player_wins {
            card_stack
                .entry(card_number + i)
                .and_modify(|x| *x += number_current_card)
                .or_insert(number_current_card);
        }
    }
    card_stack
        .into_iter()
        .filter(|(key, _)| *key <= last_card_number)
        .map(|(_, value)| value)
        .sum()
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part1() {
        let mut path = std::env::current_dir().unwrap();
        path.push("TestInput");
        path.push(format!("Day{}.txt", DAY));
        let input = std::fs::read_to_string(&path).unwrap();
        assert_eq!(solution_for_part1(&input), 13);
    }

    #[test]
    fn test_part2() {
        let mut path = std::env::current_dir().unwrap();
        path.push("TestInput");
        path.push(format!("Day{}.txt", DAY));
        let input = std::fs::read_to_string(&path).unwrap();
        assert_eq!(solution_for_part2(&input), 30);
    }

    #[test]
    fn test_final_solution() {
        let mut path = std::env::current_dir().unwrap();
        path.push("Input");
        path.push(format!("Day{}.txt", DAY));

        assert_eq!(
            solution_for_part1(&std::fs::read_to_string(&path).unwrap()),
            33950
        );
        assert_eq!(
            solution_for_part2(&std::fs::read_to_string(&path).unwrap()),
            14814534
        );
    }
}
