const DAY: u32 = 1;

pub fn run() {
    let mut path = std::env::current_dir().unwrap();
    path.push("Input");
    path.push(format!("Day{}.txt", DAY));
    let input = std::fs::read_to_string(&path).unwrap();
    println!("Solution for day {}:", DAY);
    println!("  Part 1: {}", solution_for_part1(&input));
    println!("  Part 2: {}", solution_for_part2(&input));
}

fn solution_for_part1(input: &str) -> u32 {
    let mut numbers = Vec::<u32>::new();
    for line in input.lines() {
        let numerics = line
            .chars()
            .into_iter()
            .filter(|x| x.is_numeric())
            .collect::<Vec<char>>();
        let first = numerics.first().unwrap().to_string();
        let last = numerics.last().unwrap().to_string();
        let combined = format!("{first}{last}");
        numbers.push(combined.parse().unwrap());
    }
    numbers.into_iter().sum()
}

fn solution_for_part2(input: &str) -> u32 {
    let mut numbers = Vec::<u32>::new();

    for line in input.lines() {
        let line_replaced = line
            .replace("one", "one1one")
            .replace("two", "two2two")
            .replace("three", "three3three")
            .replace("four", "four4four")
            .replace("five", "five5five")
            .replace("six", "six6six")
            .replace("seven", "seven7seven")
            .replace("eight", "eight8eight")
            .replace("nine", "nine9nine");
        let numerics = line_replaced
            .chars()
            .into_iter()
            .filter(|x| x.is_numeric())
            .collect::<Vec<char>>();
        let first = numerics.first().unwrap().to_string();
        let last = numerics.last().unwrap().to_string();
        let combined = format!("{first}{last}");
        numbers.push(combined.parse().unwrap());
    }
    numbers.into_iter().sum()
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part1() {
        let mut path = std::env::current_dir().unwrap();
        path.push("TestInput");
        path.push(format!("Day{}_Part1.txt", DAY));
        let input = std::fs::read_to_string(&path).unwrap();
        assert_eq!(solution_for_part1(&input), 142);
    }

    #[test]
    fn test_part2() {
        let mut path = std::env::current_dir().unwrap();
        path.push("TestInput");
        path.push(format!("Day{}_Part2.txt", DAY));
        let input = std::fs::read_to_string(&path).unwrap();
        assert_eq!(solution_for_part2(&input), 281);
    }

    #[test]
    fn test_final_solution() {
        let mut path = std::env::current_dir().unwrap();
        path.push("Input");
        path.push(format!("Day{}.txt", DAY));

        assert_eq!(
            solution_for_part1(&std::fs::read_to_string(&path).unwrap()),
            54390
        );
        assert_eq!(
            solution_for_part2(&std::fs::read_to_string(&path).unwrap()),
            54277
        );
    }
}
