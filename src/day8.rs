use num::integer::lcm;
use std::collections::HashMap;

const DAY: u32 = 8;

pub fn run() {
    let mut path = std::env::current_dir().unwrap();
    path.push("Input");
    path.push(format!("Day{}.txt", DAY));
    let input = std::fs::read_to_string(&path).unwrap();
    println!("Solution for day {}:", DAY);
    println!("  Part 1: {}", solution_for_part1(&input));
    println!("  Part 2: {}", solution_for_part2(&input));
}

struct Node {
    left: String,
    right: String,
}

fn solution_for_part1(input: &str) -> usize {
    let lines = input.lines().collect::<Vec<&str>>();
    let instructions = lines[0];
    let raw_nodes = &lines[2..];
    let mut nodes: HashMap<String, Node> = HashMap::with_capacity(raw_nodes.len());

    for raw_node in raw_nodes {
        let replaced = raw_node
            .replace(" = (", " ")
            .replace(", ", " ")
            .replace(')', "")
            .split(' ')
            .map(|x| x.to_string())
            .collect::<Vec<String>>();

        let node = Node {
            left: replaced.get(1).unwrap().to_string(),
            right: replaced.get(2).unwrap().to_string(),
        };

        nodes.insert(replaced.get(0).unwrap().to_string(), node);
    }

    let mut current_node = "AAA".to_string();
    let mut counter = 0_usize;
    let number_of_instructions = instructions.len();

    while current_node != "ZZZ" {
        let instruction = instructions
            .chars()
            .nth(counter % number_of_instructions)
            .unwrap();

        if instruction == 'L' {
            current_node = nodes.get(&current_node).unwrap().left.to_string();
        } else if instruction == 'R' {
            current_node = nodes.get(&current_node).unwrap().right.to_string();
        } else {
            panic!("Unknown instruction: {instruction}");
        }

        counter += 1;
    }

    counter
}

fn solution_for_part2(input: &str) -> usize {
    let lines = input.lines().collect::<Vec<&str>>();
    let instructions = lines[0];
    let raw_nodes = &lines[2..];
    let mut nodes: HashMap<String, Node> = HashMap::with_capacity(raw_nodes.len());
    let mut start_nodes = Vec::<String>::new();

    for raw_node in raw_nodes {
        let replaced = raw_node
            .replace(" = (", " ")
            .replace(", ", " ")
            .replace(')', "")
            .split(' ')
            .map(|x| x.to_string())
            .collect::<Vec<String>>();

        let node_name = replaced.get(0).unwrap().to_string();

        let node = Node {
            left: replaced.get(1).unwrap().to_string(),
            right: replaced.get(2).unwrap().to_string(),
        };

        nodes.insert(node_name.clone(), node);
        if node_name.ends_with('A') {
            start_nodes.push(node_name);
        }
    }

    let mut steps_to_z = Vec::<usize>::with_capacity(start_nodes.len());
    let number_of_instructions = instructions.len();

    for start_node in start_nodes {
        let mut current_node = start_node;
        let mut counter = 0_usize;

        while !current_node.ends_with('Z') {
            let instruction = instructions
                .chars()
                .nth(counter % number_of_instructions)
                .unwrap();

            if instruction == 'L' {
                current_node = nodes.get(&current_node).unwrap().left.to_string();
            } else if instruction == 'R' {
                current_node = nodes.get(&current_node).unwrap().right.to_string();
            } else {
                panic!("Unknown instruction: {instruction}");
            }

            counter += 1;
        }
        steps_to_z.push(counter)
    }

    let mut least_common_multiple = 1_usize;

    for num in steps_to_z {
        least_common_multiple = lcm(least_common_multiple, num);
    }

    least_common_multiple
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part1() {
        let mut path = std::env::current_dir().unwrap();
        path.push("TestInput");
        path.push(format!("Day{}_Part1.txt", DAY));
        let input = std::fs::read_to_string(&path).unwrap();
        assert_eq!(solution_for_part1(&input), 6);
    }

    #[test]
    fn test_part2() {
        let mut path = std::env::current_dir().unwrap();
        path.push("TestInput");
        path.push(format!("Day{}_Part2.txt", DAY));
        let input = std::fs::read_to_string(&path).unwrap();
        assert_eq!(solution_for_part2(&input), 6);
    }

    #[test]
    fn test_final_solution() {
        let mut path = std::env::current_dir().unwrap();
        path.push("Input");
        path.push(format!("Day{}.txt", DAY));

        assert_eq!(
            solution_for_part1(&std::fs::read_to_string(&path).unwrap()),
            20513
        );
        assert_eq!(
            solution_for_part2(&std::fs::read_to_string(&path).unwrap()),
            15995167053923
        );
    }
}
